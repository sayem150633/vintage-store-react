import React, { useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/About";
import Cart from "./pages/Cart";
import Checkout from "./pages/Checkout";
import Error from "./pages/Error";
import Login from "./pages/Login";
import Products from "./pages/Products";
import ProductDetails from "./pages/ProductDetails";
import Header from "./components/Header";
import { connect } from "react-redux";
import { authCheckState } from "./redux/auth/actions";
import Alert from "./components/Alert";
import PrivateRoute from "./components/PrivateRoute";

function App(props) {
  useEffect(() => {
    console.log("enter");
    props.onTryAutoSignup();
  }, []);
    return (
      <Router>
        <Header {...props} />
        <Alert />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/cart" component={() => <Cart {...props} />}>
            {/* <Cart /> */}
          </Route>

          {/* <Route path="/checkout" component={() => <Checkout />}>

          </Route> */}
          
            <PrivateRoute path="/checkout" {...props}>
              <Checkout />
            </PrivateRoute>
 
          <Route path="/login">
            <Login />
          </Route>
          <Route exact path="/products">
            <Products />
          </Route>
          <Route path="/products/:id">
            <ProductDetails />
          </Route>
          <Route path="*">
            <Error />
          </Route>
        </Switch>
      </Router>
    );
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.authReducer.token !== null,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onTryAutoSignup: () => dispatch(authCheckState()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
