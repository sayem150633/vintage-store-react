import React, {useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { requestProducts } from "../../redux/actions";
import Loading from "../../components/Loading";
import ProductList from "../../components/Products/ProductList";

export default function FeaturedProducts() {
  const dispatch = useDispatch();
  const onRequestProducts = () => dispatch(requestProducts());
  const state = useSelector((state) => state.requestProducts);
  useEffect(() => {
    onRequestProducts();
  }, []);

  const { products, isPending } = state;

  if (isPending) {
    return <Loading />;
  }
  return <ProductList title="Featured products" products={products.filter(item => item.featured === true)} />;;
}
