import React from "react";
import {Link} from 'react-router-dom'
import logo from '../assets/logo.svg'
import CartLink from '../components/Cart/CartLink'
// import {connect} from 'react-redux'
import {logout} from '../redux/auth/actions'
import { useDispatch } from "react-redux";

export default function Header(props) {
  const dispatch = useDispatch();
  const onRequestLogout = () => dispatch(logout());
  let username = localStorage.getItem("user");
  return (
    <header className="header">
      <img src={logo} alt="shop" className="logo" />
      <nav>
        <ul>
          <div>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
            <li>
              <Link to="/products">Products</Link>
            </li>
            {props.isAuthenticated ? <li>
              <Link to="/checkout">Checkout</Link>
            </li> : null}
            
          </div>

          {username ? <p>Welcome To {username}</p> : null}

          <div>
            {props.isAuthenticated ? (
              <li>
                <Link to="/" onClick={onRequestLogout}>
                  Logout
                </Link>
              </li>
            ) : (
              <li>
                <Link to={{
                  pathname:"/login",
                  hash: '#submit'
                }}>Login</Link>
              </li>
            )}

            <CartLink />
          </div>
        </ul>
      </nav>
    </header>
  );
}
