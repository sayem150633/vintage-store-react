import React from "react";
import { FaAngleUp, FaAngleDown } from "react-icons/fa";
import {connect} from 'react-redux'
import { removeCart, increaseCart, decreaseCart } from "../../redux/cart/actions";


function CartItem({ id, image, title, price, amount, remove, increaseAmount, decreaseAmount }) {
  // const removeItem = id => {
  //   let filterCart = [...cart].filter((item) => item.id !== id);
  //   console.log(filterCart);
  //   return filterCart
  // }
  console.log(title)
  return (
    <article className="cart-item">
      <img src={image} alt={title} />
      <div>
        <h4>{title}</h4>
        <h5>${price}</h5>
        <button
          type="button"
          className="cart-btn remove-btn"
          onClick={() => {
            remove(id);
            // console.log('remove item')
          }}
        >
          remove
        </button>
      </div>
      <div>
        <button
          type="button"
          className="cart-btn amount-btn"
          onClick={() => {
            increaseAmount(id);
            // console.log("increase item");
          }}
        >
          <FaAngleUp />
        </button>
        <p className="item-amount">{amount}</p>
        <button
          type="button"
          className="cart-btn amount-btn"
          onClick={() => {
            decreaseAmount(id, amount);
            // console.log("decrease item");
          }}
        >
          <FaAngleDown />
        </button>
      </div>
    </article>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    remove: (id) => dispatch(removeCart(id)),
    increaseAmount: (id) => dispatch(increaseCart(id)),
    decreaseAmount: (id, amount) => dispatch(decreaseCart(id, amount))
  };
}

export default connect(null,mapDispatchToProps) (CartItem)