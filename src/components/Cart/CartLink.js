import React, {useEffect} from "react";
import {Link} from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux";
import { requestCart } from "../../redux/cart/actions";

export default function CartLink() {
  const dispatch = useDispatch();
  const onRequestCart = () => dispatch(requestCart());
  const cartState = useSelector((state) => state.requestCart);
  useEffect(() => {
    console.log("Rendered!");
    onRequestCart();
  }, []);
  const { cart } = cartState;
  console.log(cart.amount);
  let newCartItems = cart.reduce((total, cartItem) => {
    return (total += cartItem.amount);
  }, 0);
  return (
    <div className="cart-link-container">
      <Link to="/cart">cart</Link>
      <div className="cart-link-total">{newCartItems}</div>
    </div>
  );
}
