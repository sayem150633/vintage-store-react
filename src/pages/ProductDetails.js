import React, {useEffect} from "react";
import {useParams, useHistory} from 'react-router-dom'
import { connect } from "react-redux";
// import {useDispatch, useSelector} from 'react-redux'
import {requestProducts} from '../redux/actions'
import {addCart} from '../redux/cart/actions'
import Loading from "../components/Loading";

function ProductDetails({ onRequestProducts, addToCart, productState }) {
  // const dispatch = useDispatch()
  // const onRequestProducts = () => dispatch(requestProducts())
  // const addToCart = (product) => dispatch(addCart(product))
  // const productState = useSelector((state) => state.requestProducts);

  useEffect(() => {
    console.log("Rendered!");
    onRequestProducts();
    // addToCart();
    // if (addToCart.length === 0) {
    //   addToCart();
    // }
  }, []);
  const { id } = useParams();
  const history = useHistory();
  const { products } = productState;
  const product = products.find((item) => item.id === parseInt(id));
  if (products.length === 0) {
    return <Loading />;
  } else {
    const { image, title, price, description } = product;
    return (
      <section className="single-product">
        <img src={image} alt={title} className="single-product-image" />
        <article>
          <h1>{title}</h1>
          <h2>${price}</h2>
          <p>{description}</p>
          <button
            className="btn btn-primary btn-block"
            onClick={() => {
              // add to cart
              addToCart(product);
              history.push("/cart");
            }}
          >
            add to cart
          </button>
        </article>
      </section>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    productState: state.requestProducts,
  };
}
const mapDispatchToProps = dispatch => {
  return {
    addToCart: (product) => dispatch(addCart(product)),
    onRequestProducts: () => dispatch(requestProducts()),
  };
  
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);
