import React, { useEffect } from "react";
import { connect } from "react-redux";
import { requestProducts } from "../redux/actions";

const mapStateToProps = (state) => {
  return {
    products: state.products,
    isPending: state.isPending,
    error: state.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onRequestProducts: () => dispatch(requestProducts()),
  };
};

class Products extends React.Component {
  componentDidMount() {
    this.props.onRequestProducts();
  }
  render() {
    console.log(this.props.products);
    return <h1>hello from products page</h1>;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);
