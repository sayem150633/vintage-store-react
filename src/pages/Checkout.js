import React from "react";
import {useHistory} from 'react-router-dom'
import EmptyCart from '../components/Cart/EmptyCart'
import {connect} from 'react-redux'
import {CardElement, StripeProvider, Elements, injectStripe} from 'react-stripe-elements'
import {useDispatch} from "react-redux"
import {setOrder} from '../redux/checkout/actions'
import {removeCartItems} from '../redux/cart/actions'

function Checkout( {cart} ) {
  const [name, setName] = React.useState("")
  const [error, setError] = React.useState("")

  const dispatch = useDispatch()
  const history = useHistory()

  const isEmpty = !name
   function handleSubmit (e) {
    e.preventDefault()
    console.log(name, cart)
    let body = {
      name, cart, total: newTotal
    }
    dispatch(setOrder(body))
    dispatch(removeCartItems())
    history.push("/")
  }
  console.log(cart)
  let newTotal = cart.reduce((total, cartItem) => {
    return (total += cartItem.amount * cartItem.price);
  }, 0);
  if(cart.length < 1) return <EmptyCart/>
    return (
      <section className="section form">
        <h2 className="section-title">checkout</h2>
        <form className="checkout-form">
          <h3>
            order total: <span>${newTotal}</span>
          </h3>
          <div className="form-control">
            <label htmlFor="name">name</label>
            <input
              type="text"
              value={name}
              onChange={(e) => {
                setName(e.target.value);
              }}
            />
          </div>
          <div className="stripe-input">
            <label htmlFor="card-element">Credit or Debit Cart</label>
            <p className="stripe-info">
              Test using this credit card: <span>4242 4242 4242 4242</span>
              <br />
              enter any 5 digits for zip code
            </p>
          </div>
          {/* <CardElement className="card-element"></CardElement> */}
          {error && <p className="form-empty">{error}</p>}
          {isEmpty ? (
            <p className="form-empty">Please fill out name field</p>
          ) : (
            <button
              type="submit"
              onClick={handleSubmit}
              className="btn btn-primary btn-block"
            >
              submit
            </button>
          )}
        </form>
      </section>
    );
}

// const CardForm = injectStripe(Checkout)

// const StripeWrapper = () => {
//   return (
//     <StripeProvider apiKey="pk_test_51IRGw2I6bphQ8rgKjmXJCUM9KCQbpFFAF3XHDZiVeRGADze9g44PqbdmLuWrRMmLIvBABSP5WY1dkPHtdIGFiOA300KD8DLlc9">
//       <Elements>
//         <CardForm></CardForm>
//       </Elements>
//     </StripeProvider>
//   )
// }

const mapStateToProps = state => {
  return {
    cart: state.requestCart.cart,
  };
}

export default connect(mapStateToProps) (Checkout)


