import React, {useEffect} from "react";
import {useDispatch, useSelector} from 'react-redux'
import {requestCart} from '../redux/cart/actions'
import EmptyCart from '../components/Cart/EmptyCart'
import CartItem from '../components/Cart/CartItem'
import {Link} from 'react-router-dom'

export default function Cart() {
  let user = false;
  const dispatch = useDispatch()
  const onRequestCart = () => dispatch(requestCart())
  const cartState = useSelector(state => state.requestCart)
  useEffect(() => {
    console.log("Rendered!");
    onRequestCart();
  }, [cart]);
  const {cart} = cartState

 
  if(cart.length === 0){
    return <EmptyCart/>
  } else {
    console.log(cart)
    let newCartItems = cart.reduce((total, cartItem) => {
      return (total += cartItem.amount);
    }, 0);
     let newTotal = cart.reduce((total, cartItem) => {
       return (total += cartItem.amount * cartItem.price);
     }, 0);
     newTotal = parseFloat(newTotal.toFixed(2));
     console.log(newCartItems);
  return (
    <section className="cart-items section">
      <h2>your cart</h2>
      {cart.map((item) => {
        return <CartItem key={item.id} {...item} />;
      })}
      <h2>total: ${newTotal}</h2>
      {user ? (
        <Link to="/checkout" className="btn btn-primary btn-block">
          checkout
        </Link>
      ) : (
        <Link to="/login" className="btn btn-primary btn-block">
          login
        </Link>
      )}
    </section>
  );
      }
}
