import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import {Provider} from 'react-redux'
import {createStore, applyMiddleware, combineReducers, compose} from 'redux'
import thunkMiddleware from "redux-thunk";
import {requestProducts} from './redux/reducers'
import {requestCart} from './redux/cart/reducers'
import {authReducer} from './redux/auth/reducers'

const composeEnhances = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const rootReducer = combineReducers({requestProducts, requestCart, authReducer})
const store = createStore(
  rootReducer,
  composeEnhances(applyMiddleware(thunkMiddleware)
));

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
