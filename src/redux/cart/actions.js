import {
  ADD_TO_CART,
  REQUEST_CART_FAILED,
  REQUEST_CART_PENDING,
  REQUEST_CART_SUCCESS,
  REMOVE_CART_ITEM,
  INCREASE_CART_ITEM,
  DECREASE_CART_ITEM,
  REMOVE_CART_ITEMS
} from "./constants";
// import LocalCart from "../../utils/localCart"

function getCartFromLocalStorage() {
  return localStorage.getItem("cart")
    ? JSON.parse(localStorage.getItem("cart"))
    : [];
}

export const requestCart = () => (dispatch) => {
  console.log(getCartFromLocalStorage());
  dispatch({ type: REQUEST_CART_PENDING });
  dispatch({ type: REQUEST_CART_SUCCESS, payload: getCartFromLocalStorage() });
  // dispatch({ type: ADD_TO_CART, payload: product });
  dispatch({ type: REQUEST_CART_FAILED, payload: "error" });
};

export const removeCart = (id) => (dispatch) => {
  dispatch({ type: REMOVE_CART_ITEM, payload: id });
};

export const increaseCart = (id) => (dispatch) => {
  dispatch({ type: INCREASE_CART_ITEM, payload: id });
};

export const decreaseCart = (id, amount) => (dispatch) => {
  if (amount === 1) {
    dispatch(removeCart(id));
  } else {
    dispatch({ type: DECREASE_CART_ITEM, payload: id });
  }
};

export const addCart = (product) => (dispatch) => {
  dispatch(requestCart());
  dispatch({ type: ADD_TO_CART, payload: product });
};

export const removeCartItems = () => (dispatch) => {
    dispatch({ type: REMOVE_CART_ITEMS })
};
