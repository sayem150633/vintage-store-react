import {
  ADD_TO_CART,
  REMOVE_CART_ITEM,
  REQUEST_CART_SUCCESS,
  REQUEST_CART_PENDING,
  REQUEST_CART_FAILED,
  INCREASE_CART_ITEM,
  DECREASE_CART_ITEM,
  REMOVE_CART_ITEMS
} from "./constants";

const initialStateCart = {
  isPending: false,
  cart: [],
  error: "",
};

export const requestCart = (state = initialStateCart, action = {}) => {
  switch (action.type) {
    case REQUEST_CART_PENDING:
      return Object.assign({}, state, { isPending: true });

    case REQUEST_CART_SUCCESS:
      console.log(action.payload);
      return Object.assign({}, state, {
        cart: action.payload,
        isPending: false,
      });

    case REQUEST_CART_FAILED:
      return Object.assign({}, state, {
        error: action.payload,
        isPending: true,
      });

    case REMOVE_CART_ITEM:
      let newCart = [...state.cart].filter(
        (item) => item.id !== action.payload
      );
      localStorage.setItem("cart", JSON.stringify(newCart));
      return { ...state, cart: newCart };
    // return Object.assign({}, state, {
    //   cart: [...state.cart].filter(item => item.id !== action.payload)
    // });

    case INCREASE_CART_ITEM:
      const incCart = [...state.cart].map((item) => {
        return item.id === action.payload
          ? { ...item, amount: item.amount + 1 }
          : { ...item };
      });
      localStorage.setItem("cart", JSON.stringify(incCart));
      return { ...state, cart: incCart };

    case DECREASE_CART_ITEM:
      const decCart = [...state.cart].map((item) => {
        return item.id === action.payload
          ? { ...item, amount: item.amount - 1 }
          : { ...item };
      });
      localStorage.setItem("cart", JSON.stringify(decCart));
      console.log(decCart);
      return { ...state, cart: decCart };

    case ADD_TO_CART:
      let cart = state.cart;
      console.log(action.payload);
      const { id, title, image, price } = action.payload;
      const item = [...state.cart].find((item) => item.id === id);
      if (item) {
        const incCart = [...state.cart].map((item) => {
          return item.id === id
            ? { ...item, amount: item.amount + 1 }
            : { ...item };
        });
        localStorage.setItem("cart", JSON.stringify(incCart));
        return { ...state, cart: incCart };
      } else {
        const newItem2 = {
          id,
          title,
          image,
          price: parseInt(price),
          amount: 1,
        };
        cart.push(newItem2);
        localStorage.setItem("cart", JSON.stringify(cart));
        console.log(cart);
        return {
          ...state,
          cart: cart,
        };
      }
    case REMOVE_CART_ITEMS:
      let cartState = state.cart;
      cartState = [];
      localStorage.setItem("cart", JSON.stringify(cartState));
      return {
        ...state,
        cart: cartState,
      };

    // console.log(action.payload)
    //   const {id, title, image, price} = action.payload
    //   // const item = [...state.cart].find(item => item.id === id);
    //   const newItem2 = {
    //         id,
    //         title,
    //         image,
    //         price: parseInt(price),
    //         amount: 1,
    //       };
    //       const newCartItem = [...state.cart, newItem2]
    //       console.log(newCartItem)
    //       return { ...state, cart: newCartItem };
    // return Object.assign({}, state, { cart: newCartItem });
    // if(item){
    //     const incCart = [...state.cart].map((item) => {
    //       return item.id === action.payload
    //         ? { ...item, amount: item.amount + 1 }
    //         : { ...item };
    //     });
    //     return { ...state, cart: incCart };
    // } else {

    // }

    default:
      return state;
  }
};
