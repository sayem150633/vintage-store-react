import {
  AUTH_LOGOUT,
  AUTH_FAIL,
  AUTH_SUCCESS,
  AUTH_START,
  ALERT_DANGER,
  ALERT_SUCCESS,
  ALERT_HIDE,
} from "./constants";


const initialState = {
    token: null,
    error: null,
    loading: false,
    show: false,
    msg: '',
}

export const authReducer = (state=initialState, action={}) => {
    switch(action.type){
        case AUTH_START:
            return Object.assign({}, state, {
                loading: true,
                error: null,
                show: "accessing user data. please wait..."
            })

        case AUTH_SUCCESS:
            return Object.assign({}, state, {
                token: action.token,
                error: null,
                loading: false
            })

        case AUTH_FAIL:
            return Object.assign({}, state, {
                error: action.error,
                loading: false
            })

        case AUTH_LOGOUT:
            return Object.assign({}, state, {
                token: null
            })

        case ALERT_SUCCESS:
            return Object.assign({}, state, {msg: action.payload, show: true})

        case ALERT_DANGER:
            return Object.assign({}, state, {msg: action.payload, show: true})

        case ALERT_HIDE:
            return Object.assign({}, state, {show: false})


        default:
            return state
    }
}


