import {SET_ORDER} from "./contants"
import axios from 'axios'

export const setOrder = (payload) => (dispatch) => {
    console.log(payload)
    let id = localStorage.getItem('id')
    console.log(id)
    axios.post("http://127.0.0.1:8000/order/", {
        name: payload.name,
        items: payload.cart,
        total: payload.total,
        user: id
    }).then(res => console.log(res))
    .catch(err => console.log(err))
}