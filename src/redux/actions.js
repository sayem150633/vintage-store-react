import {
  REQUEST_PRODUCTS_PENDING,
  REQUEST_PRODUCTS_SUCCESS,
  REQUEST_PRODUCTS_FAILED,
} from "./constants";
import Url from "../utils/URL";

export const requestProducts = () => (dispatch) => {
    dispatch({ type: REQUEST_PRODUCTS_PENDING });
      fetch(`${Url}/api/products/`)
        .then((response) => response.json())
        .then((data) =>
          dispatch({ type: REQUEST_PRODUCTS_SUCCESS, payload: data })
        )
        .catch((err) =>
          dispatch({ type: REQUEST_PRODUCTS_FAILED, payload: err })
        );
}

// export const requestProducts =  () => {
//   return async dispatch => {
//     dispatch({ type: REQUEST_PRODUCTS_PENDING });
//      await fetch(`${Url}/api`)
//       .then((response) => response.json())
//       .then((data) =>
//         dispatch({ type: REQUEST_PRODUCTS_SUCCESS, payload: data })
//       )
//       .catch((err) =>
//         dispatch({ type: REQUEST_PRODUCTS_FAILED, payload: err })
//       );
//   };
// };
