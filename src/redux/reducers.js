import {REQUEST_PRODUCTS_SUCCESS, REQUEST_PRODUCTS_PENDING, REQUEST_PRODUCTS_FAILED} from './constants'


const initialStateProducts = {
    isPending: false,
    products: [],
    error: ''
}

export const requestProducts = (state=initialStateProducts, action={}) => {
    switch (action.type) {
      case REQUEST_PRODUCTS_PENDING:
        return Object.assign({}, state, { isPending: true });

      case REQUEST_PRODUCTS_SUCCESS:
        return Object.assign({}, state, {
          products: action.payload,
          isPending: false,
        });

      case REQUEST_PRODUCTS_FAILED:
        return Object.assign({}, state, {
          error: action.payload,
          isPending: true,
        });

      default:
        return state;
    }
}

